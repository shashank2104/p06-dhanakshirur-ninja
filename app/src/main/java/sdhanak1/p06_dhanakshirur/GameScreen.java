package sdhanak1.p06_dhanakshirur;

import sdhanak1.p06_dhanakshirur.util.SystemUiHider;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class GameScreen extends Activity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    /**
     * The instance of the {@link SystemUiHider} for this activity.
     */
  //  private SystemUiHider mSystemUiHider;

    AnimatorSet fruitAnimation;
    Animation animFadeIn;
    Animation rotation;
    TextView score;
    TextView lives;
    MediaPlayer background;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game_screen);

  /*      final View controlsView = findViewById(R.id.fullscreen_content_controls);
        final View contentView = findViewById(R.id.fullscreen_content);

        // Set up an instance of SystemUiHider to control the system UI for
        // this activity.
        mSystemUiHider = SystemUiHider.getInstance(this, contentView, HIDER_FLAGS);
        mSystemUiHider.setup();
        mSystemUiHider
                .setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
                    // Cached values.
                    int mControlsHeight;
                    int mShortAnimTime;

                    @Override
                    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
                    public void onVisibilityChange(boolean visible) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                            // If the ViewPropertyAnimator API is available
                            // (Honeycomb MR2 and later), use it to animate the
                            // in-layout UI controls at the bottom of the
                            // screen.
                            if (mControlsHeight == 0) {
                                mControlsHeight = controlsView.getHeight();
                            }
                            if (mShortAnimTime == 0) {
                                mShortAnimTime = getResources().getInteger(
                                        android.R.integer.config_shortAnimTime);
                            }
                            controlsView.animate()
                                    .translationY(visible ? 0 : mControlsHeight)
                                    .setDuration(mShortAnimTime);
                        } else {
                            // If the ViewPropertyAnimator APIs aren't
                            // available, simply show or hide the in-layout UI
                            // controls.
                            controlsView.setVisibility(visible ? View.VISIBLE : View.GONE);
                        }

                        if (visible && AUTO_HIDE) {
                            // Schedule a hide().
                            delayedHide(AUTO_HIDE_DELAY_MILLIS);
                        }
                    }
                });

        // Set up the user interaction to manually show or hide the system UI.
        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TOGGLE_ON_CLICK) {
                    mSystemUiHider.toggle();
                } else {
                    mSystemUiHider.show();
                }
            }
        });
*/
        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

        background = MediaPlayer.create(getApplicationContext(), R.raw.background);
        background.start();
        background.setLooping(true);
        final ImageView melon = (ImageView) findViewById(R.id.melon);
        final ImageView orange = (ImageView) findViewById(R.id.orange);
        final ImageView apple = (ImageView) findViewById(R.id.apple);
        final ImageView banana = (ImageView) findViewById(R.id.banana);
        final ImageView pineapple = (ImageView) findViewById(R.id.pineapple);

        final ImageView bomb = (ImageView) findViewById(R.id.bomb);

        score = (TextView)findViewById(R.id.score);
        lives = (TextView)findViewById(R.id.life);

        createFruit(melon);
        createFruit(apple);
        createFruit(banana);
        createFruit(orange);
        createFruit(pineapple);

        createFruit(bomb);

        checkIfSliced(melon);
        checkIfSliced(apple);
        checkIfSliced(banana);
        checkIfSliced(orange);
        checkIfSliced(pineapple);

        checkIfSliced(bomb);
    }

    public void createFruit(final ImageView fruit) {

        fruit.postDelayed(new Runnable() {
            @Override
            public void run() {
                fruit.setVisibility(View.VISIBLE);
            }
        }, 3000);

//        rotation = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        switch (fruit.getTag().toString()) {
            case "melon":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.fruit_animation);
                break;
            case "apple":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.apple_animation);
                break;
            case "orange":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.orange_animation);
                break;
            case "banana":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.banana_animation);
                break;
            case "pineapple":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.pineapple_animation);
                break;
            case "bomb":
                fruitAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(this, R.animator.pineapple_animation);
                break;
            default:
                break;
        }

        //      rotation.setFillAfter(true);
    //    rotation.setRepeatCount(Animation.INFINITE);
     //   rotation.setDuration(5000);
        fruitAnimation.setTarget(fruit);
        fruitAnimation.start();
       // fruit.startAnimation(rotation);
    }


    public void checkIfSliced(final ImageView fruit) {
        fruit.setOnTouchListener(new SwipeGesture(this) {
            @Override
            public void onSwipeLeft() {
                if (fruit.getTag().toString().equalsIgnoreCase("bomb")) {
                    fruit.setVisibility(View.INVISIBLE);
                    int numberOfLives = Integer.valueOf(lives.getText().toString());
                    numberOfLives = numberOfLives - 1;
                    lives.setText(String.valueOf(numberOfLives));
                    if (numberOfLives == 0) {
                        gameOver(null);
                    } else {
                        createFruit(fruit);
                    }
                } else {
                    fruit.setVisibility(View.INVISIBLE);
                    int num1 = Integer.valueOf(score.getText().toString());
                    num1 += 20;
                    score.setText(String.valueOf(num1));
                    createFruit(fruit);
                }
            }

            public void onSwipeRight() {
                if (fruit.getTag().toString().equalsIgnoreCase("bomb")) {
                    fruit.setVisibility(View.INVISIBLE);
                    int numberOfLives = Integer.valueOf(lives.getText().toString());
                    numberOfLives = numberOfLives - 1;
                    lives.setText(String.valueOf(numberOfLives));
                    if (numberOfLives == 0) {
                        gameOver(null);
                    } else {
                        createFruit(fruit);
                    }
                } else {
                    fruit.setVisibility(View.INVISIBLE);
                    int num1 = Integer.valueOf(score.getText().toString());
                    num1 += 20;
                    score.setText(String.valueOf(num1));
                    createFruit(fruit);
                }
            }

            public void onSwipeTop() {
                if (fruit.getTag().toString().equalsIgnoreCase("bomb")) {
                    fruit.setVisibility(View.INVISIBLE);
                    int numberOfLives = Integer.valueOf(lives.getText().toString());
                    numberOfLives = numberOfLives - 1;
                    lives.setText(String.valueOf(numberOfLives));
                    if (numberOfLives == 0) {
                        gameOver(null);
                    } else {
                        createFruit(fruit);
                    }
                } else {
                    fruit.setVisibility(View.INVISIBLE);
                    int num1 = Integer.valueOf(score.getText().toString());
                    num1 += 20;
                    score.setText(String.valueOf(num1));
                    createFruit(fruit);
                }
            }

            public void onSwipeBottom() {
                if (fruit.getTag().toString().equalsIgnoreCase("bomb")) {
                    fruit.setVisibility(View.INVISIBLE);
                    int numberOfLives = Integer.valueOf(lives.getText().toString());
                    numberOfLives = numberOfLives - 1;
                    lives.setText(String.valueOf(numberOfLives));
                    if (numberOfLives == 0) {
                        gameOver(null);
                    } else {
                        createFruit(fruit);
                    }
                } else {
                    fruit.setVisibility(View.INVISIBLE);
                    int num1 = Integer.valueOf(score.getText().toString());
                    num1 += 20;
                    score.setText(String.valueOf(num1));
                    createFruit(fruit);
                }
            }

        });
    }

    public void gameOver(View view) {
            background.stop();
            Intent intent = new Intent(this, GameOver.class);
            intent.putExtra("keep", false);
            this.finish();
            startActivity(intent);
    }
}
